require "util"
require("prototypes.item")
require("prototypes.recipe")
require("prototypes.fluid")
require("prototypes.technology")
data.raw["item"]["iron-ore"].pictures = {
      { size = 64, filename = "__base__/graphics/icons/iron-ore.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=31/255, g=31/255, b=71/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=31/255, g=31/255, b=71/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=31/255, g=31/255, b=71/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=31/255, g=31/255, b=71/255}}
}
data.raw["item"]["copper-ore"].pictures = {
      { size = 64, filename = "__base__/graphics/icons/copper-ore.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=71/255, g=31/255, b=31/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=71/255, g=31/255, b=31/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=71/255, g=31/255, b=31/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=71/255, g=31/255, b=31/255}}
    }
data.raw["item"]["stone"].pictures = {
      { size = 64, filename = "__base__/graphics/icons/stone.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=51/255, g=51/255, b=51/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=51/255, g=51/255, b=51/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=51/255, g=51/255, b=51/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=51/255, g=51/255, b=51/255}}
    }
data.raw["item"]["uranium-ore"].pictures =
	{
	  {
		layers =
		{
		  {
			filename = "__base__/graphics/icons/uranium-ore.png",
			size = 64,
			scale = 0.25,
			mipmap_count = 4,
			tint = {r=31/255, g=71/255, b=31/255}
		  },
		  {
			filename = "__base__/graphics/icons/uranium-ore.png",
			blend_mode = "additive",
			draw_as_light = true,
			tint = {r = 3/40, g = 3/40, b = 3/40, a = 3/40},
			size = 64,
			scale = 0.25,
			mipmap_count = 4
		  },
		}
	  },
	  {
		layers =
		{
		  {
			filename = "__base__/graphics/icons/uranium-ore-1.png",
			size = 64,
			scale = 0.25,
			mipmap_count = 4,
			tint = {r=31/255, g=71/255, b=31/255}
		  },
		  {
			filename = "__base__/graphics/icons/uranium-ore-1.png",
			blend_mode = "additive",
			draw_as_light = true,
			tint = {r = 3/40, g = 3/40, b = 3/40, a = 3/40},
			size = 64,
			scale = 0.25,
			mipmap_count = 4
		  },
		}
	  },
	  {
		layers =
		{
		  {
			filename = "__base__/graphics/icons/uranium-ore-2.png",
			size = 64,
			scale = 0.25,
			mipmap_count = 4,
			tint = {r=31/255, g=71/255, b=31/255}
		  },
		  {
			filename = "__base__/graphics/icons/uranium-ore-2.png",
			blend_mode = "additive",
			draw_as_light = true,
			tint = {r = 3/40, g = 3/40, b = 3/40, a = 3/40},
			size = 64,
			scale = 0.25,
			mipmap_count = 4
		  },
		}
	  },
	  {
		layers =
		{
		  {
			filename = "__base__/graphics/icons/uranium-ore-3.png",
			size = 64,
			scale = 0.25,
			mipmap_count = 4,
			tint = {r=31/255, g=71/255, b=31/255}
		  },
		  {
			filename = "__base__/graphics/icons/uranium-ore-3.png",
			blend_mode = "additive",
			draw_as_light = true,
			tint = {r = 3/40, g = 3/40, b = 3/40, a = 3/40},
			size = 64,
			scale = 0.25,
			mipmap_count = 4
		  },
		}
	  }
	}

