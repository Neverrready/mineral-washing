data:extend({
    {
        type="item",
        name="ore-polishing-crystal",
        stack_size = 100,
        icon = "__base__/graphics/icons/stone.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/stone.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=229/255, g=224/255, b=217/255,a=20/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=229/255, g=224/255, b=217/255,a=20/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=229/255, g=224/255, b=217/255,a=20/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=229/255, g=224/255, b=217/255,a=20/255}}
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="hardened-ore-polishing-crystal",
        stack_size = 100,
        icon = "__base__/graphics/icons/stone-brick.png",
        icon_size = 64,
        icon_mipmaps =4,
        pictures = {
            {size = 64, filename = "__base__/graphics/icons/stone-brick.png", scale = 0.25,mipmap_count=4,tint = {r=229/255, g=224/255, b=217/255,a=20/255}}
        },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255,a=22/255}
    },
    {
        type="item",
        name="tier-1-washed-copper-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/copper-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/copper-ore.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}}
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="tier-2-washed-copper-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/copper-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/copper-ore.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/copper-ore-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}}
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="tier-3-washed-copper-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/copper-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/copper-ore.png",   scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/copper-ore-1.png", scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/copper-ore-2.png", scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/copper-ore-3.png", scale = 0.25, mipmap_count = 4 }
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
	{
        type="item",
        name="tier-1-washed-stone",
        stack_size = 100,
        icon = "__base__/graphics/icons/stone.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/stone.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}}
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="tier-2-washed-stone",
        stack_size = 100,
        icon = "__base__/graphics/icons/stone.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/stone.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/stone-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}}
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="tier-3-washed-stone",
        stack_size = 100,
        icon = "__base__/graphics/icons/stone.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/stone.png",   scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/stone-1.png", scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/stone-2.png", scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/stone-3.png", scale = 0.25, mipmap_count = 4 }
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
	},
    {
        type="item",
        name="tier-1-washed-uranium-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/uranium-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore.png",
			tint = {r=81/255, g=81/255, b=81/255},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = {r = 6/40, g = 6/40, b = 6/40, a = 6/40},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-1.png",
			tint = {r=81/255, g=81/255, b=81/255},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-1.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = { r = 6/40, g = 6/40, b = 6/40, a = 6/40},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-2.png",
			tint = {r=81/255, g=81/255, b=81/255},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-2.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = { r = 6/40, g = 6/40, b = 6/40, a = 6/40},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-3.png",
			tint = {r=81/255, g=81/255, b=81/255},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-3.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = { r = 6/40, g = 6/40, b = 6/40, a = 6/40},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      }
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="tier-2-washed-uranium-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/uranium-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore.png",
			tint = {r=135/255, g=135/255, b=135/255}
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = {r = 9/40, g = 9/40, b = 9/40, a = 9/40},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-1.png",
			tint = {r=135/255, g=135/255, b=135/255}
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-1.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = {r = 9/40, g = 9/40, b = 9/40, a = 9/40},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-2.png",
			tint = {r=135/255, g=135/255, b=135/255}
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-2.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = {r = 9/40, g = 9/40, b = 9/40, a = 9/40},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-3.png",
			tint = {r=135/255, g=135/255, b=135/255}
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-3.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = {r = 9/40, g = 9/40, b = 9/40, a = 9/40},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      }
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="tier-3-washed-uranium-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/uranium-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore.png",
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = {r = 0.3, g = 0.3, b = 0.3, a = 0.3},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-1.png",
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-1.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = { r = 0.3, g = 0.3, b = 0.3, a = 0.3},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-2.png",
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-2.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = { r = 0.3, g = 0.3, b = 0.3, a = 0.3},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      },
      {
        layers =
        {
          {
            filename = "__base__/graphics/icons/uranium-ore-3.png",
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
          {
            filename = "__base__/graphics/icons/uranium-ore-3.png",
            blend_mode = "additive",
            draw_as_light = true,
            tint = { r = 0.3, g = 0.3, b = 0.3, a = 0.3},
            size = 64,
            scale = 0.25,
            mipmap_count = 4
          },
        }
      }
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
	},
    {
        type="item",
        name="tier-1-washed-iron-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/iron-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/iron-ore.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=81/255, g=81/255, b=81/255}}
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="tier-2-washed-iron-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/iron-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/iron-ore.png",   scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-1.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-2.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}},
      { size = 64, filename = "__base__/graphics/icons/iron-ore-3.png", scale = 0.25, mipmap_count = 4 ,tint = {r=135/255, g=135/255, b=135/255}}
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    },
    {
        type="item",
        name="tier-3-washed-iron-ore",
        stack_size = 100,
        icon = "__base__/graphics/icons/iron-ore.png",
        icon_size = 64,
        icon_mipmaps = 4,
    pictures =
    {
      { size = 64, filename = "__base__/graphics/icons/iron-ore.png",   scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/iron-ore-1.png", scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/iron-ore-2.png", scale = 0.25, mipmap_count = 4 },
      { size = 64, filename = "__base__/graphics/icons/iron-ore-3.png", scale = 0.25, mipmap_count = 4 }
    },
        subgroup = "intermediate-product",
        tint = {r=128/255, g=0/255, b=0/255}
    }

})